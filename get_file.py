import logging
import os
import pandas as pd

#Check_nomenclature
def check_nomenclature(file_name):
    if file_name.startswith('BD'):
        print("nomenclature true")
        return True
    else:
        print("nomenclature false")
        return False

#Get_period
def get_period(file_name):
    period_str = file_name[2:6]
    period = int(period_str)
    print (period)
    return period

# Extract_sheets
def extract_sheets(file_path, file_name, out):
    file = os.path.join(file_path, file_name)
    x = pd.ExcelFile(file)

    for sheet_name in x.sheet_names:
        df = pd.read_excel(file, sheet_name)
        out_file = os.path.join(out, f"{sheet_name}.xls")

        # Write the DataFrame to an Excel file using the 'openpyxl'
        #'openpyxl' Used for reading and writing Excel files in the '.xlsx' format.
        df.to_excel(out_file, index=False, engine='openpyxl')
        print(f"Sheet '{sheet_name}' successfully extracted to '{out_file}'")

# Get_file
def get_file(file_path, file_name, out):
    if check_nomenclature(file_name):
        print("nomenclature")
        file = os.path.join(file_path, file_name)
        data = pd.read_excel(file)
        # kwargs['ti'].xcom_push(key='data', value=df)
        period = get_period(file_name)
        print(f"Period: {period}")

        if extract_sheets(file_path, file_name, out):
            print("Sheets successfully extracted.")
        else:
            print("Sheet extraction failed.")

        return "Data read successfully."

#get_file(file_path, file_name)
#check_nomenclature(file_name)
#get_period(file_name)
#extract_sheets(file_path, file_name, out)












