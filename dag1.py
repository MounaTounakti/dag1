import os
from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python import PythonOperator
from modules.get_file import get_file

default_args = {
    'owner': 'Airflow',
    'start_date': datetime(2024, 3, 10),
    'retries': 1,
    'retry_delay': timedelta(seconds=5)
}

#file_path = "/lib/airflow/app/dags/in/2023"
#file_name = "BD2023.xls"
#out = "/lib/airflow/app/dags/out"
current_directory = os.getcwd()
file_path = os.path.join(current_directory, 'app', 'dags', 'in', '2023')
file_name = "BD2023.xls"
out =  os.path.join(current_directory, 'app', 'dags', 'out')

with DAG('Dag_Nomenclature', default_args=default_args, schedule=timedelta(days=1), catchup=False) as dag:

    get_file_task = PythonOperator(
        task_id='get_file',
        python_callable=get_file,
        op_args=[file_path, file_name, out],
        provide_context=True,
        dag=dag,
    )

get_file_task

